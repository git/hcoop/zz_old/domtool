(*
Domtool (http://hcoop.sf.net/)
Copyright (C) 2004  Adam Chlipala

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(* Main domtool structure *)

signature DOMTOOL =
sig
    val dprint : string -> unit
    (* If Config.debug, then it prints the argument followed by a newline. *)
    val inputLine : TextIO.instream -> string option
    (* Like TextIO.inputLine, but skips past lines commented out with #'s. *)

    type map = string StringMap.map
    type set = StringSet.set


    val error : string * string -> unit
    (* (error (path, msg)) signals an error about path with description msg. *)

    (* Handlers for configuration files *)
    type handlerData = {path : string,   (* Path to the config file *)
			domain : string, (* Domain in which it lives *)
			parent : string, (* Parent of that domain *)
			vars : map,      (* Current variable values *)
			paths : set,     (* Authorized filesystem paths *)
			users : set,     (* Authorized UNIX users *)
			groups : set}    (* Authorized UNIX groups *)
    type mkdomData = {path : string,
		      domain : string}
    type handler = {init : unit -> unit,
		    file : handlerData -> unit,
		    finish : unit -> unit,
		    publish : unit -> OS.Process.status,
		    mkdom : mkdomData -> OS.Process.status}

    val setVhostHandler : handler -> unit
    (* Set the handler for virtual host configuration files *)
    val setHandler : string * handler -> unit
    (* Set the handler to be used for files with a specific name *)


    val read : unit -> OS.Process.status
    (* Command line domtool entry point *)
    val publish : unit -> OS.Process.status
    (* Propagate changes to real daemons *)
    val mkDom : string * string -> OS.Process.status
    (* (mkDom (dom, user)) creates a new domain record *)
end
