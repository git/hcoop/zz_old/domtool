(*
Domtool (http://hcoop.sf.net/)
Copyright (C) 2004  Adam Chlipala

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(* Administration of Courier IMAP virtual mailboxes *)

structure Vmail :> VMAIL =
struct
    open Config VmailConfig Util

    fun main args =
	case args of
	    domain::rest =>
	    if validDomain domain then
		let
		    val tooldir = dataDir ^ toDir domain
		    val maildir = mailboxDir ^ "/" ^ domain
		in
		    if not (Posix.FileSys.access (tooldir, [])) then
			(print ("Domain not in domtool (would be " ^ tooldir ^ ")\n");
			 OS.Process.failure)
		    else if not (Posix.FileSys.access (tooldir, [Posix.FileSys.A_WRITE])) then
			(print "Access denied\n";
			 OS.Process.failure)
		    else
			case rest of
			    ["list"] =>
			    (let
				 val dir = Posix.FileSys.opendir maildir
					   
				 fun loop () =
				     case Posix.FileSys.readdir dir of
					 NONE => ()
				       | SOME user => (print (user ^ "\n");
						  loop ())
			     in
				 loop ();
				 Posix.FileSys.closedir dir;
				 OS.Process.success
			     end handle Io => OS.Process.success)
			  | ["add", user] =>
			    if validUser user then
				let
				    val _ = if Posix.FileSys.access (maildir, []) then
						()
					    else
						ignore (OS.Process.system ("mkdir " ^ maildir))
						
				    val umaildir = maildir ^ "/" ^ user
				in
				    if Posix.FileSys.access (umaildir, []) then
					(print "Mailbox already exists\n";
					 OS.Process.failure)
				    else if OS.Process.system (courierDir ^ "/bin/maildirmake " ^ umaildir) = OS.Process.success
					    andalso OS.Process.system (sudo ^ " " ^ courierDir ^ "/sbin/userdb \"" ^ domain ^ "/" ^ user ^ "@" ^ domain ^ "\" set home=/home/vmail mail=/home/vmail/" ^ domain ^ "/" ^ user ^ " uid=1042 gid=1043") = OS.Process.success
					    andalso OS.Process.system (sudo ^ " " ^ courierDir ^ "/sbin/userdbpw | " ^ sudo ^ " " ^ courierDir ^ "/sbin/userdb \"" ^ domain ^ "/" ^ user ^ "@" ^ domain ^ "\" set systempw") = OS.Process.success then
					(print "Mailbox created\n";
					 OS.Process.success)
				    else
					(print "Error creating mailbox\n";
					 OS.Process.failure)
				end
			    else
				(print "Invalid mailbox name\n";
				 OS.Process.failure)
			  | ["passwd", user] =>
			    if not (validUser user) then
				(print "Invalid mailbox name\n";
				 OS.Process.failure)
			    else if not (Posix.FileSys.access (maildir ^ "/" ^ user, [])) then
				(print "Mailbox does not exist\n";
				 OS.Process.failure)
			    else if OS.Process.system (sudo ^ " " ^ courierDir ^ "/sbin/userdbpw | " ^ sudo ^ " " ^ courierDir ^ "/sbin/userdb \"" ^ domain ^ "/" ^ user ^ "@" ^ domain ^ "\" set systempw") = OS.Process.success then
				(print "Password set\n";
				 OS.Process.success)
			    else
				(print "Unable to set password\n";
				 OS.Process.failure)
			  | _ => (print "Unknown command\n";
				  OS.Process.failure)
		end
	    else
		(print "Invalid domain\n";
		 OS.Process.failure)
	  | _ => (print "Invalid command\n";
		  OS.Process.failure)
end