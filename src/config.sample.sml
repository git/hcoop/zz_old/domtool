(*
Domtool (http://hcoop.sf.net/)
Copyright (C) 2004  Adam Chlipala

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(* Some options you might want to change *)

structure Config =
struct
    (* Root of domain configuration tree *)
    val dataDir = "/etc/domains"

    (* Temporary location for new daemon config files before publishing *)		   
    val scratchDir = "/home/domains"

    (* Name of a directory to create/delete for locking purposes *)
    val lockFile = "/home/domains/.lock"

    (* UID of the user as whom domtool will run (used to check authenticity
     * of .users, .groups, and .paths files) *)
    val uid = 1031

    (* Path to the config file for the default web vhost for this server.
     * Configuration for this host will be ignored, leaving it up to admins to
     * set it up manually.
     *) 
    val defaultWebPath = "/etc/domains/net/hcoop/www"

    (* When this is true, extra debug info may be printed. *)
    val debug = false

    (* Command to run dompub (probably as root) *)
    val dompub = "sudo /usr/local/bin/domtool.real publish"

    (* Paths to standard tools *)
    val cp = "/bin/cp"
    val diff = "/usr/bin/diff"
    val cat = "/bin/cat"
    val sudo = "/usr/bin/sudo"
end
