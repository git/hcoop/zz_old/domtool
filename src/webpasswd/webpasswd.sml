(*
Domtool (http://hcoop.sf.net/)
Copyright (C) 2004  Adam Chlipala

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(* Utility to let a user set a password in an Apache password file for a user
 * name matches his UNIX login name. *)

structure Webpasswd :> WEBPASSWD =
struct
    open Config ApacheConfig WebpasswdConfig

    fun main () =
	let
	    val uid = SysWord.toInt (Posix.ProcEnv.uidToWord (Posix.ProcEnv.getuid ()))

	    val proc = Unix.execute("/bin/sh", ["-c", getent ^ " passwd " ^
						      Int.toString uid])
	    val (stdout, stdin) = Unix.streamsOf proc

	    val name =
		case String.tokens (fn ch => ch = #":") (TextIO.inputLine stdout) of
		    name::_ => name
		  | _ => raise Fail "Unexpected getent output"
	in
	    TextIO.closeOut stdin;
	    TextIO.closeIn stdout;
	    if not (OS.Process.isSuccess (Unix.reap proc)) then
		(print "Error reaping getent\n";
		 OS.Process.failure)
	    else
		(print ("Update password for " ^ name ^ "\n");
		 OS.Process.system (htpasswd ^ " " ^ passwdFile ^ " \"" ^ name ^ "\""))
	end
end

