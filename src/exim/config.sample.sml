(*
Domtool (http://hcoop.sf.net/)
Copyright (C) 2004  Adam Chlipala

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(* Some options you might want to change *)

structure EximConfig =
struct
    val aliasesFile = "/etc/aliases"
    (* File for first pass of e-mail aliases *)

    val aliasesDefaultFile = "/etc/aliases.default"
    (* File for second pass of e-mail aliases *)

    val eximBase = "/etc/exim/exim.base"
    (* Base exim configuration *)

    val eximFile = "/etc/exim/exim.conf"
    (* Exim config file *)

    val pubCommand = "/etc/init.d/exim reload"
    (* Command to publish data *)
end
