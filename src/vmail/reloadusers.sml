(*
Domtool (http://hcoop.sf.net/)
Copyright (C) 2004  Adam Chlipala

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(* Utility to reload Courier IMAP data after modifying virtual mailboxes *)

structure Reloadusers :> RELOADUSERS =
struct
    open VmailConfig

    fun main _ =
	let
	    val proc = Unix.execute("/bin/sh", ["-c", courierDir ^ "/sbin/pw2userdb"])
	    val (stdout, stdin) = Unix.streamsOf proc

	    val outf = TextIO.openOut (userdbDir ^ "/localhost")

	    fun loop () =
		case TextIO.inputLine stdout of
		    "" => ()
		  | line =>
		    let
			val tokens = String.tokens (fn ch => Char.isSpace ch orelse ch = #"|" orelse ch = #"=") line

			fun findHome (n::v::rest) =
			    if n = "home" then
				TextIO.output (outf, "|mail=" ^ v ^ "/Maildir")
			    else
				findHome rest
			  | findHome _ = ()
		    in
			TextIO.output (outf, String.substring (line, 0, size line - 1));
			case tokens of
			    first::rest =>
			    (case Int.fromString first of
				 NONE => findHome rest
			       | _ => ())
			  | _ => ();
			TextIO.output (outf, "\n");
			loop ()
		    end
	in
	    loop ();
	    TextIO.closeOut outf;
	    TextIO.closeIn stdout;
	    TextIO.closeOut stdin;
	    Unix.reap proc;
	    if OS.Process.isSuccess (OS.Process.system (courierDir ^ "/sbin/makeuserdb")) then
		OS.Process.system postReload
	    else
		(print "Error running makeuserdb\n";
		 OS.Process.failure)
	end
end
