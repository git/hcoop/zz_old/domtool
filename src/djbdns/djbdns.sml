(*
Domtool (http://hcoop.sf.net/)
Copyright (C) 2004  Adam Chlipala

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(* Djbdns DNS mapping config *)

structure Djbdns :> DJBDNS =
struct
    open Config DjbdnsConfig Util

    val ldHandler = ref (fn _ : string => ())
    fun setLocalDomainHandler f = ldHandler := f

    val dns = ref (NONE : TextIO.outstream option)

    fun init () = dns := SOME (TextIO.openOut (scratchDir ^ "/data.shared"))
    fun finish () = (TextIO.closeOut (valOf (!dns));
		     dns := NONE)

    fun handler {path, domain, parent, vars, paths, users, groups} =
	let
	    val _ = Domtool.dprint ("Reading dns " ^ path ^ " for " ^ parent ^ "....")

	    val dns = valOf (!dns)

	    val al = TextIO.openIn path

	    val hasEmail = ref false

	    fun loop (line, mxnum) =
		let
		    fun err () = (Domtool.error (path, "Invalid entry: " ^ trimLast line);
				  mxnum)
		in
		    case String.tokens Char.isSpace line of
			[] => mxnum
		      | ["Default", addr] =>
			(case resolveAddr (vars, addr) of
			     "" => err ()
			   | addr => (TextIO.output (dns, "=" ^ parent ^ ":" ^ addr ^ "\n");
				      mxnum))
		      | [ty, host, addr] =>
			let
			    val pre =
				(case ty of
				     "Primary" => "."
				   | "Secondary" => "&"
				   | "Host" => "="
				   | "Alias" => "+"
				   | "Mail" => "@"
				   | "Redir" => "C"
				   | _ => "")
			in
			    if pre = "C" then
				(case resolveDomain (vars, addr) of
				     "" =>  err ()
				   | host' =>
				     if validHost host then
					 (TextIO.output (dns, pre ^ host ^ "." ^ parent ^ ":" ^ addr ^ "\n");
					  mxnum)
				     else
					 err ())
			    else case (resolveAddr (vars, addr), pre) of
				     ("", _) => err ()
				   | (addr, ".") =>
				     if validHost host then
					 (TextIO.output (dns, pre ^ parent ^ ":" ^ addr ^ ":" ^ host ^ "." ^ parent ^ "\n");
					  mxnum)
				     else
					 err ()
				   | (addr, "&") =>
				     if validHost host then
					 (TextIO.output (dns, pre ^ parent ^ ":" ^ addr ^ ":" ^ host ^ "." ^ parent ^ "\n");
					  mxnum)
				     else
					 err ()
				   | (addr, "@") =>
				     (if not (!hasEmail) then
					  (hasEmail := true;
					   !ldHandler parent)
				      else
					  ();
					  if validHost host then
					      (TextIO.output (dns, pre ^ parent ^ ":" ^ addr ^ ":" ^ host ^ "." ^ parent ^ ":" ^ Int.toString mxnum ^ "\n");
					       mxnum+1)
					  else
					      err ())
				   | (addr, "=") =>
				     if validHost host then
					 (TextIO.output (dns, pre ^ host ^ "." ^ parent ^ ":" ^ addr ^ "\n");
					  mxnum)
				     else
					 err ()
				   | (addr, "+") =>
				     if validHost host then
					 (TextIO.output (dns, pre ^ host ^ "." ^ parent ^ ":" ^ addr ^ "\n");
					  mxnum)
				     else
					 err ()
				   | _ => err ()
			end
		      | _ => err ()
		end
	in
	    ioLoop (fn () => Domtool.inputLine al) loop 0;
	    TextIO.closeIn al
	end handle Io => Domtool.error (path, "IO error")

    fun publish () =
	if OS.Process.isSuccess (OS.Process.system
	       (diff ^ " " ^ scratchDir ^ "/data.shared " ^ dataFile)) then
	    OS.Process.success
	else if not (OS.Process.isSuccess (OS.Process.system
	       (cp ^ " " ^ scratchDir ^ "/data.shared " ^ dataFile))) then
	    (print "Error copying data.shared\n";
	     OS.Process.failure)
	else if OS.Process.isSuccess (OS.Process.system pubCommand) then
	    OS.Process.success
	else
	    (print "Error publishing data.shared\n";
	     OS.Process.failure)

    fun mkdom {path, ...} = OS.Process.system (cp ^ " " ^ defaultFile ^ " " ^ path ^ "/.dns")

    val _ = Domtool.setHandler (".dns", {init = init,
					 file = handler,
					 finish = finish,
					 publish = publish,
					 mkdom = mkdom})
end
