(*
Domtool (http://hcoop.sf.net/)
Copyright (C) 2004  Adam Chlipala

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(* Apache vhost management module, with Webalizer support *)

structure Apache :> APACHE =
struct
    open Config ApacheConfig Util

    val vhosts = ref (NONE : TextIO.outstream option)

    fun init () = vhosts := SOME (TextIO.openOut (scratchDir ^ "/vhosts.conf"))
    fun finish () = (TextIO.closeOut (valOf (!vhosts));
		     vhosts := NONE)

    fun handler {path, domain, parent, vars, paths, users, groups} =
	let
	    val _ = Domtool.dprint ("Reading host " ^ path ^ " for " ^ domain ^ "....")

	    val vhosts = valOf (!vhosts)

	    val hf = TextIO.openIn path
	    val rewrite = ref false

	    val conf = TextIO.openOut (wblConfDir ^ "/" ^ domain ^ ".conf")
	    val _ = TextIO.output (conf, "LogFile\t" ^ logDir ^ domain ^ "-access.log\n" ^
					 "OutputDir\t" ^ wblDocDir ^ "/" ^ domain ^ "\n" ^
					 "HostName\t" ^ domain ^ "\n" ^
					 "HideSite\t" ^ domain ^ "\n" ^
					 "HideReferrer\t" ^ domain ^ "\n")

	    val dir = wblDocDir ^ "/" ^ domain
	    val _ =
		if Posix.FileSys.access (dir, []) then
		    ()
		else
		    Posix.FileSys.mkdir (dir, Posix.FileSys.S.flags [Posix.FileSys.S.ixoth, Posix.FileSys.S.irwxu,
								     Posix.FileSys.S.irgrp, Posix.FileSys.S.iwgrp])

	    val htac = TextIO.openOut (dir ^ "/.htaccess")
	    val user = ref defaultUser
	    val group = ref defaultGroup

	    fun loop (line, ()) =
		(case String.tokens Char.isSpace line of
		     [] => ()
		   | ["User", user'] =>
		     if StringSet.member (users, user') then
			 user := user'
		     else
			 Domtool.error (path, "not authorized to run as " ^ user')
		   | ["Group", group'] =>
		     if StringSet.member (groups, group') then
			 group := group'
		     else
			  Domtool.error (path, "not authorized to run as group " ^ group')
		   | ["ServerAdmin", email] => TextIO.output (vhosts, "\tServerAdmin " ^ email ^ "\n")
		   | ["UserDir"] => TextIO.output (vhosts, "\tUserDir public_html\n\t<Directory /home/*/public_html/cgi-bin>\n\t\tAllowOverride None\n\t\tOptions ExecCGI\n\t\tAllow from all\n\t\tSetHandler cgi-script\n\t</Directory>\n\tScriptAliasMatch ^/~(.*)/cgi-bin/(.*) /home/$1/public_html/cgi-bin/$2\n")
		   | ["DocumentRoot", p] =>
		     if checkPath (paths, p) then
			 TextIO.output (vhosts, "\tDocumentRoot " ^ p ^ "\n")
		     else
			 print (path ^ ": not authorized to use " ^ p ^ "\n")
		   | "RewriteRule" :: args =>
		     (if not (!rewrite) then
			  (rewrite := true;
			   TextIO.output (vhosts, "\tRewriteEngine on\n"))
		      else
			  ();
			  TextIO.output (vhosts, foldl (fn (a, s) => s ^ " " ^ a) "\tRewriteRule" args ^ "\n"))
		   | ["Alias", from, to] =>
		     if checkPath (paths, to) then
			 TextIO.output (vhosts, "\tAlias " ^ from ^ " " ^ to ^ "\n")
		     else
			 Domtool.error (path, "not authorized to use " ^ to)
		   | "ErrorDocument" :: code :: rest =>
		     TextIO.output (vhosts, foldl (fn (a, s) => s ^ " " ^ a) ("\tErrorDocument " ^ code) rest ^ "\n")
		   | ["ScriptAlias", from, to] =>
		     if checkPath (paths, to) then
			 TextIO.output (vhosts, "\tScriptAlias " ^ from ^ " \"" ^ to ^ "\"\n")
		     else
			 Domtool.error (path, "not authorized to use " ^ to)
		   | ["SSI"] =>
		     TextIO.output (vhosts, "\t<Location />\n\t\tOptions +Includes +IncludesNOEXEC\n\t</Location>\n")
		   | ["ServerAlias", dom] =>
		     if validDomain dom then
			 let
			     val file = foldr (fn (c, s) => s ^ "/" ^ c) dataDir (String.fields (fn ch => ch = #".") dom) ^ ".aliased"
			 in
			     if Posix.FileSys.access (file, []) then
				 (TextIO.output (vhosts, "\tServerAlias " ^ dom ^ "\n");
				  TextIO.output (conf, "HideSite\t" ^ dom ^ "\n" ^
						       "HideReferrer\t" ^ dom ^ "\n"))
			     else
				 Domtool.error (path, "not authorized to ServerAlias " ^ dom)
			 end
		     else
			 Domtool.error (path, "bad host: " ^ dom)
		   | "WebalizerUsers" :: users =>
		     TextIO.output (htac, "AuthType Basic\n" ^
					  "AuthName \"Abulafia web account\"\n" ^
					  "AuthUserFile " ^ passwdFile ^ "\n" ^
					  foldl (fn (u, s) => s ^ " " ^ u) "Require user" users ^ "\n")
		   | ["AbuPrivate"] => TextIO.output (vhosts,
						      "\t<Location />\n" ^
						      "\t\tAuthName \"Abulafia web account\"\n" ^
						      "\t\tAuthType basic\n" ^
						      "\t\tAuthUserFile " ^ passwdFile ^ "\n" ^
						      "\t\tRequire valid-user\n" ^
						      "\t\tOrder Deny,Allow\n" ^
						      "\t\tDeny from all\n" ^
						      "\t\tAllow from 127.0.0.1\n" ^
						      "\t\tAllow from 63.246.10.45\n" ^
						      "\t\tSatisfy any\n" ^
						      "\t</Location>\n")
		   | ["Default"] => (TextIO.output (vhosts, "\tServerAlias " ^ parent ^ "\n");
				     TextIO.output (conf, "HideSite\t" ^ parent ^ "\n" ^
							  "HideReferrer\t" ^ parent ^ "\n"))
		   | ["CGI", p] =>
		     if checkPath (paths, p) then
			 TextIO.output (vhosts, "\t<Directory " ^ p ^ ">\n" ^
						"\t\tOptions ExecCGI\n" ^
						"\t\tSetHandler cgi-script\n" ^
						"\t</Directory>\n")
		     else
			 Domtool.error (path, "not authorized to use " ^ p)
		   | ["HTML", p] =>
		     if checkPath (paths, p) then
			 TextIO.output (vhosts, "\t<Directory " ^ p ^ ">\n" ^
						"\t\tForceType text/html\n" ^
						"\t</Directory>\n")
		     else
			 Domtool.error (path, "not authorized to use " ^ p)
		   | cmd::_ => Domtool.error (path, "unknown option: " ^ cmd))
	in
	    TextIO.output (vhosts, "<VirtualHost *>\n" ^
				   "\tServerName " ^ domain ^ "\n" ^
				   "\tErrorLog " ^ logDir ^ domain ^ "-error.log\n" ^
				   "\tCustomLog " ^ logDir ^ domain ^ "-access.log combined\n" ^
				   "\tIndexOptions FancyIndexing FoldersFirst\n");
	    ioLoop (fn () => Domtool.inputLine hf) loop ();
	    TextIO.output (vhosts, "\tUser ");
	    TextIO.output (vhosts, !user);
	    TextIO.output (vhosts, "\n\tGroup ");
	    TextIO.output (vhosts, !group);
	    TextIO.output (vhosts, "\n</VirtualHost>\n\n");
	    TextIO.closeIn hf;
	    TextIO.closeOut conf;
	    TextIO.closeOut htac
	end handle Io => Domtool.error (path, "IO error")

    fun publish () =
	if OS.Process.isSuccess (OS.Process.system
	       (diff ^ " " ^ scratchDir ^ "/vhosts.conf " ^ dataFile)) then
	    OS.Process.success
	else if not (OS.Process.isSuccess (OS.Process.system
	       (cp ^ " " ^ scratchDir ^ "/vhosts.conf " ^ dataFile))) then
	    (print "Error copying vhosts.conf\n";
	     OS.Process.failure)
	else if OS.Process.isSuccess (OS.Process.system pubCommand) then
	    OS.Process.success
	else
	    (print "Error publishing vhosts.conf\n";
	     OS.Process.failure)

    fun mkdom _ = OS.Process.success

    val _ = Domtool.setVhostHandler {init = init,
				     file = handler,
				     finish = finish,
				     publish = publish,
				     mkdom = mkdom}
end

