all: domtool webpasswd vmail reloadusers

domtool:
	mlton -output bin/domtool src/domtool.cm

webpasswd:
	mlton -output bin/webpasswd src/webpasswd/webpasswd.cm

vmail:
	mlton -output bin/vmail src/vmail/vmail.cm

reloadusers:
	mlton -output bin/reloadusers src/vmail/reloadusers.cm