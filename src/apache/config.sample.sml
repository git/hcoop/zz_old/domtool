(*
Domtool (http://hcoop.sf.net/)
Copyright (C) 2004  Adam Chlipala

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(* Some options you might want to change *)

structure ApacheConfig =
struct
    val logDir = "/var/log/apache/"
    (* Where Apache writes log files *)

    val wblConfDir = "/etc/webalizer"
    (* Directory containing Webalizer .conf files *)

    val wblDocDir = "/var/www/users.hcoop.net/html/webalizer"
    (* Directory to use for outputting Webalizer results *)

    val passwdFile = "/etc/apache/passwds"
    (* Default system web password file *)		     

    val defaultUser = "www-data"
    (* Default UNIX user to allow a vhost to run as *)

    val defaultGroup = "www-data"
    (* Default UNIX group to allow a vhost to run as *)

    val dataFile = "/etc/apache/vhosts.conf"
    (* Location of real vhosts configuration file *)

    val pubCommand = "/etc/init.d/apache reload"
    (* Command to publish changes *)
end
